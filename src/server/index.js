import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import mongoose from 'mongoose';
import resolvers from "./resolvers/index";
import typeDefs from "./typeDefs/index";
import session from "express-session";
import connectRedis from "connect-redis";
import config from "./config";
import path from "path";

const { NODE_ENV, PORT, DB_NAME, DB_HOST, DB_USER, DB_PASS, SESSION_NAME, SESSION_SECRET, 
    SESSION_LIFETIME_MS, SESSION_LIFETIME_S, SESSION_LIFETIME_M, SESSION_LIFETIME_H,
    REDIS_HOST, REDIS_PORT, REDIS_PASS } = config;

const startServer = async () => {

    const app = express();
    
    const RedisStore = connectRedis(session);
    const store = new RedisStore({
      host: REDIS_HOST,
      port: REDIS_PORT,
      pass: REDIS_PASS
    });

    const sessLifeTime = 1000 * 60 * 60 * parseInt(SESSION_LIFETIME_H);
    app.use(session({
      store,
      name: SESSION_NAME,
      secret: SESSION_SECRET,
      resave: true,
      rolling: true,
      saveUninitialized: false,
      cookie: {
        maxAge: sessLifeTime,
        sameSite: true,
        secure: NODE_ENV === 'production'
      }
    }));
    
    const server = new ApolloServer({
        // These will be defined for both new or existing servers
        typeDefs,
        resolvers,
        playground: NODE_ENV === 'development'? {
          settings: {
            'request.credentials': 'include'
          }
        }: false,
        context: ({ req, res }) => ({ req, res })
    });

    
    const whitelist = ['http://localhost:3000', 'http://localhost:4000', 'https://farivsblog.herokuapp.com'];
    const delegateCors = (origin, callback) => {
      if ( origin ) {
        let originMod =  origin.endsWith('/') ? origin.slice(0, origin.length-1) : origin;
        if (whitelist.indexOf(originMod) !== -1 || !originMod) callback(null, true)
        else callback(new Error(originMod + ': Not allowed by CORS'))
      } else callback(null, false);
    }
    
    const corsOption = {
      origin: delegateCors,
      credentials: true,
      optionsSuccessStatus: 200
    }
    server.applyMiddleware({ app, cors: corsOption }); // app is from an existing express app
    
    // await mongoose.connect(DB_URL+DB_NAME, {useNewUrlParser: true});
    // await mongoose.connect('mongodb://mongo:27017/'+DB_NAME, {useNewUrlParser: true});
    await mongoose.connect(DB_HOST+DB_NAME, { auth: { authSource: DB_NAME }, user: DB_USER, pass: DB_PASS, useNewUrlParser: true });

    app.use(express.static('public'));
    app.get('*', ( req, res) => {
        res.sendFile( path.resolve(__dirname, '../public', 'index.html') )
    });
    
    const port = PORT || 4000
    app.listen({ port }, () =>
      console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`)
    )
};

startServer();