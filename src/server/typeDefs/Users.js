import { gql } from "apollo-server-express";
// The GraphQL schema
export const users = gql`
  type Query {
    me: Users
    users: [Users!]!
    loginResponse(
      email: String!
      password: String!
      ip: String
    ): LoginResponse
  }

  type LoginResponse {
    id: ID
    email: String
    username: String
    profile_image: String
    ip: String
  }

  type Users {
    id: ID
    username: String
    firstname: String
    lastname: String
    email: String
    profile_image: String
    ip: String
  }
  type Mutation {
    createUser(
      username: String!
      firstname: String
      lastname: String!
      email: String!
      password: String!
      profile_image: String
      ip: String
    ): Users!
    logout: Boolean
  }
`;