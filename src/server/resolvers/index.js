import { userResolvers } from "./Users";

const resolvers = [ userResolvers ];
export default resolvers;