import bcrypt from "bcrypt";
import {Users} from "../models/Users";
import * as Auth from "../auth";
import userValidation from "../object_schemas/index";
import Joi from "@hapi/joi";

// A map of functions which return data for the schema.
const saltRounds = 5;

function getIP ( req )
{
  let ip = (req.headers['x-forwarded-for'] || '').split(',').pop() || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress
  return ip;
}

export const userResolvers = {
  Query: {
    me: async (_, args, { req }, info) => {
      await Auth.checkLoggeedIn( req );
      return await Users.findById(req.session.userId);
    },
    users: () => Users.find(),
    loginResponse: async (_, { email, password, ip }, { req }) => {

      const { userId } = req.session;
      if ( userId ) return await Users.findById( userId )

      ip = getIP( req );
      console.log( ip );
      await Joi.validate({ email, password, ip }, userValidation.login, { abortEarly: false });

      let userData = await Users.findOne({email});
      if ( ! userData ) throw new Error("Wrong Email");
      
      const isPaasswordTrue = await bcrypt.compare(password, userData.password);
      if ( ! isPaasswordTrue ) throw new Error("Wrong Password");

      req.session.userId = userData.id;

      return userData;
    } 
  },
  Mutation: {
    createUser: async (_, { username, firstname, lastname, email, password, profile_image, ip }, { req }) => {
      await Auth.checkLoggedOut( req );

      let args = { username, firstname, lastname, email, password, profile_image, ip };
      await Joi.validate(args, userValidation.registration, { abortEarly: false });

      let hashedPassword = await bcrypt.hash(password, saltRounds);
      args.password = hashedPassword;
      let userData = new Users(args);
      await userData.save();
      req.session.userId = userData.id;
      return userData;
    },
    logout : (_, args, context, info) => {
      Auth.checkLoggeedIn( context.req );
      return Auth.logout( context.req, context.res );
    }
  }
};