import mongoose from "mongoose";
export const Users = mongoose.model('Users', { 
    username: String,
    firstname: String,
    lastname: String,
    email: String,
    password: String,
    profile_image: String,
    ip: String
});