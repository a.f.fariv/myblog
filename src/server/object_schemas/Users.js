import Joi from "@hapi/joi";

const username = Joi.string().alphanum().min(3).max(30).required().label('Username');
const firstname = Joi.string().alphanum().allow('').label('First name');
const lastname = Joi.string().alphanum().min(3).max(30).required().label('Last name');
const email = Joi.string().email({ minDomainSegments: 2 }).required().label('Email');
let password = Joi.string().regex(/^(?=\S*[a-z])(?=\S*[A-Z])(?=\S*\d)(?=\S*[^\w\s])\S{8,50}$/).label('Password').options({
                    language: {
                        string: {
                            regex: {
                                base: 'Must have a lowercase letter, an uppercase letter, one digit, a special characters and at least have 8 characters and not more than 50 characters'
                            }
                        }
                    }
                });
const profile_image = Joi.string().allow('', null).label('Profile image');
const ip = Joi.string().required().label('IP');

const registration = Joi.object().keys({
    username,
    firstname,
    lastname,
    email,
    password,
    profile_image,
    ip
});

password = Joi.string().min(8).max(50).required().label('Password');
const login = Joi.object().keys({
    email,
    password,
    ip
});

export default { registration, login };