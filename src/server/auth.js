import { AuthenticationError } from "apollo-server-core";
import config from "./config";
const { SESSION_NAME } = config;

const signedIn = req => req.session.userId;

export const checkLoggeedIn = req => {
    if ( ! signedIn( req ) ) {
        throw new AuthenticationError('You must be logged in.');
    }
}

export const checkLoggedOut = req => {
    if ( signedIn( req ) ) {
        throw new AuthenticationError('You are already logged in.');
    }
}
console.log( SESSION_NAME );
export const logout = (req, res) => new Promise((resolve, reject) => {
    req.session.destroy(err => {
        if ( err ) reject( err );
        res.clearCookie( SESSION_NAME ); 
        resolve(true);
    })
});