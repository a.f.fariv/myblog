import React, { Fragment } from 'react';
import { makeStyles } from "@material-ui/core/styles";
import Logo from "./pages/subComponents/Logo";
import clsx from 'clsx';
import {NavLink} from 'react-router-dom';
import MeQuery from "../ClientQueries/me";
import LogoutMutationQuey from "../ClientQueries/logout";
import { Query } from "react-apollo";

const CHECK_LOGGED_IN_QUERY = MeQuery;

function Nav( properties ) {
    const client = properties.client;
    console.log( properties );
    console.log( client );
    const useStyles = makeStyles(theme => ({
        root: {
            justifyContent: 'center',
            alignItems: 'center',
            display: 'flex'
        },
        navbar: {
            flexDirection: 'row',
            justifyContent: 'space-around',
            backgroundColor: theme.palette.primary.main,
        },
        navlink: {
            width: '10%',
            justifyContent: 'space-around',
            listStyle: 'none',
            margin: theme.spacing(0),
        },
        link: {
            cursor: 'pointer',
            textDecoration: 'none',
            color: 'white',
            fontWeight: 700,
            fontSize: 14,
            padding: theme.spacing(2, 1),
            "&:hover": {
                //you want this to be the same as the backgroundColor above
                backgroundColor: theme.palette.primary.light
            }
        }
    }));
    console.log( properties );
    const logoutFunc = async () => {
        
        const LOGOUT_MUTATION_QUERY = LogoutMutationQuey;            
        const IsLoggedOut = await client.mutate({
            mutation: LOGOUT_MUTATION_QUERY
        });
        console.log( IsLoggedOut );
        if ( IsLoggedOut.data.logout ) {
            console.log( 'a' );
            console.log( properties );

            await properties.history.push('/');        
            console.log( 'b' );
            await client.resetStore();
            // window.open('http://localhost:3000', '_self');
            console.log( properties );
            console.log( 'c' );
            console.log( 'd' );
        }

    }

    const classes = useStyles();
    return (
        <Fragment>
            <div className={clsx(classes.root)}>
                <Logo />
            </div>
            <div className={clsx(classes.root, classes.navbar)}>
                <ul className={clsx(classes.root, classes.navlink)}>
                    <Query query={CHECK_LOGGED_IN_QUERY}>
                    {({ loading, error, data, startPolling, stopPolling }) => {
                        console.log( error );
                        startPolling(50);
                        if ( loading ) return "loaading...";
                        console.log( data );
                        console.log( ! data );
                        setTimeout( () => stopPolling(), 300 );
                        if ( ! data ) {
                            return (
                                <React.Fragment>
                                    <NavLink exact activeStyle={{color: "black", backgroundColor: "white"}} to="/register" className={clsx(classes.link)}><li>Register</li></NavLink>
                                    <NavLink exact activeStyle={{color: "black", backgroundColor: "white"}} to="/login" className={clsx(classes.link)}><li>Login</li></NavLink>
                                </React.Fragment>
                            )
                        }
                        return (
                            <React.Fragment>
                                <NavLink exact activeStyle={{color: "black", backgroundColor: "white"}} to="/profile" className={clsx(classes.link)}><li>Profile</li></NavLink>
                                {/* <NavLink exact activeStyle={{color: "black", backgroundColor: "white"}} to="/logout" className={clsx(classes.link)}><li>Logout</li></NavLink> */}
                                <li className={clsx(classes.link)} onClick={logoutFunc}>Logout</li>
                            </React.Fragment>
                        )
                    }}
                    </Query>
                </ul>
            </div>
        </Fragment>
    );
}
 
export default Nav;