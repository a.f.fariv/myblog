import React from 'react';
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import clsx from 'clsx';
import RegisterForm from "./RegisterForm";

function Register( {client, ...props} ) {

    const useStyles = makeStyles(theme => ({
        root: {
          display: 'flex',
          flexWrap: 'wrap',
        },
        margin: {
          margin: theme.spacing(2)
        },
        centerLogin: {
            alignItems: 'center',
            justifyContent: 'center'
        }
    }));

    const classes = useStyles();
    
    return (
        <Grid container>
            <Grid className={clsx(classes.root, classes.centerLogin, classes.margin)} item xs={12}>
                <RegisterForm {...props} client={client}/>
            </Grid>
        </Grid>
    );
}
 
export default Register;