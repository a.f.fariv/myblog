import React from 'react';
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx';
import { Link } from "react-router-dom";

function Logo () {

    const useStyles = makeStyles(theme => ({
        sitenameLogo: {
            margin: theme.spacing(1, 2),
            justifyContent: 'center',
            alignItems: 'center',
            display: 'flex',
            width: '20%',
            color: theme.palette.primary.main,
            border: "2px solid " + theme.palette.primary.main,
            borderRadius: 4,
            textDecoration: 'none'
        }
    }));
    
    const classes = useStyles();

    return (
        <Link to="/" className={clsx( classes.sitenameLogo )}>
            <h3>My Blog</h3>
        </Link>
    );
}

export default Logo;