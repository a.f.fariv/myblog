import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import clsx from "clsx";

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column'
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  coverPhotoContainer: {
    flex: '1 1 auto'
  },
  coverphoto: {
    maxWidth: '100%'
  },
  profilePicContainer: {
    flex: '1 1 auto',
    justifyContent: 'center',
    alignItems: 'center'
  },
  profilePic: {
    flex: '1 1 auto',
    borderRadius: '5%',
    width: '124px',
    height: '124px',
    position: 'absolute',
    backgroundImage: 'url(https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png)',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    backgroundSize: 'cover'
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
  isActive: {
    borderRadius: '50%',
    backgroundColor: 'white',
    width: '20px',
    height: '20px',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    borderBottomLeftRadius: '5%',
    borderTopRightRadius: '5%',
  },
  isActiveInner: {
      borderRadius: '50%',
      backgroundColor: 'green',
      width: '15px',
      height: '15px',

  },
  usernameSection : {
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingTop: '82px'
  },
  userFullnameText: {
    fontSize: '17px',
    fontWeight: '500'
  },
  softText: {
    color: 'grey',
    fontSize: '14px',
    fontWeight: '300',
    marginTop: 0
  },
  toolbar: theme.mixins.toolbar,
});

function ProfileCard(props) {
    
  const { classes, userData } = props;
  return (
    <div className={classes.root}>
        <div className={clsx(classes.root, classes.coverPhotoContainer)}>
            <img className={clsx(classes.root, classes.coverphoto)} alt="cover" src="https://www.incimages.com/uploaded_files/image/970x450/getty_509107562_2000133320009280346_351827.jpg" />
        </div>
        <div className={clsx(classes.root, classes.profilePicContainer)}>
            <div className={clsx(classes.root, classes.profilePic)}>
                <div className={clsx(classes.root, classes.isActive)}>
                    <div className={clsx(classes.root, classes.isActiveInner)}></div>
                </div>
            </div>
        </div>
        <div className={clsx(classes.root, classes.usernameSection)}>
            <label className={clsx(classes.root, classes.userFullnameText)}>{userData.firstname + ' ' + userData.lastname}</label>
            <p className={clsx(classes.root, classes.softText)}>{userData.email}</p>
        </div>
    </div>
  );
}

ProfileCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProfileCard);
