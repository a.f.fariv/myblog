import React from 'react';
import { Grid } from '@material-ui/core';
import ProfileCard from "./subComponents/ProfileCard";

function Profile ( properties )
{
    const { client: { cache : { data : { data } } } } = properties;
    const UserModelId = data.ROOT_QUERY.me.id;
    const profileShadow = {
        boxShadow: '5px -2px 11px 0px rgba(63,81,181,0.67)',
    }
    if ( data[UserModelId] ) {
        return (
            <Grid container>
                <Grid item xs={4} style={profileShadow}><ProfileCard userData={data[UserModelId]} /></Grid>
                <Grid item xs={5}>My Own Posts</Grid>
                <Grid item xs={3}>Advertisements</Grid>
            </Grid>
        );
    } else {
        window.open('http://localhost:3000', '_self');
        return null
    }
}

export default Profile;