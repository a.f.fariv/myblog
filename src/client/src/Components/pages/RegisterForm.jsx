import React, { Fragment } from 'react';
import Paper from "@material-ui/core/Paper";
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon/Icon';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom";
import RegisterQuery from "../../ClientQueries/registerMutationQuery";

function RegisterForm ( { client, ...props } ) {

    const REGISTER_QUERY = RegisterQuery;

    const useStyles = makeStyles(theme => ({
        root: {
          display: 'flex',
          flexWrap: 'wrap',
          flexDirection: 'column'
        },
        margin: {
          margin: theme.spacing(2)
        },
        textField: {
            flexBasis: 200,
        },
        paperStyle: {
            flexBasis: 600,
            flexDirection: "row",
            padding: theme.spacing(2, 8),
        },
        loginButton : {
            margin: theme.spacing(1, 2)
        },
        sitenameLogo: {
            margin: theme.spacing(1, 2),
            justifyContent: 'center',
            alignItems: 'center',
            display: 'flex',
            color: theme.palette.primary.main,
            border: "2px solid " + theme.palette.primary.main,
            borderRadius: 4,
        },
        link: {
            textDecoration: 'none',
            color: 'blue'
        }
    }));

    const classes = useStyles();
    
    const [values, setValues] = React.useState({
        username: '',
        firstname: '',
        lastname: '',
        email: '',
        password: '',
        visibility: false
    });

    const handleChange = prop => event => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, visibility: !values.visibility });
    };
    
    let emailRequiredProp = {required: true, error: false}
    let passwordRequiredProp = {required: true, error: false}
    let usernameRequiredProp = {required: true, error: false}
    let firstnameRequiredProp = {required: false, error: false}
    let lastnameRequiredProp = {required: true, error: false}

    const handleLoginSubmit = async ( event ) => {
        event.preventDefault();
        
        let {username, firstname, lastname, email, password} = values;
                    
        const IsRegistered = await client.mutate({

            mutation: REGISTER_QUERY,
            variables: {
                username,
                firstname,
                lastname,
                email,
                password,
                ip: '12'
            }
        });

        if( IsRegistered.data  ) window.open('http://localhost:3000/', '_self');
    }
    
    return (
        <Fragment>
            <Paper className={clsx(classes.paperStyle)} elevation={2}>
                <div className={clsx( classes.sitenameLogo )}>
                    <h1>My Blog</h1>
                </div>
                <form action="" onSubmit={handleLoginSubmit}>
                    <TextField
                        {...firstnameRequiredProp}
                        id="firstname"
                        className={clsx(classes.root, classes.textField, classes.margin) + ' firstname'}
                        variant="outlined"
                        label="First Name"
                        value={values.firstname}
                        onChange={handleChange('firstname')}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton edge="end">
                                        <Icon>label</Icon>
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                    />
                    
                    <TextField
                        {...lastnameRequiredProp}
                        id="lastname"
                        className={clsx(classes.root, classes.textField, classes.margin) + ' lastname'}
                        variant="outlined"
                        label="Last Name"
                        value={values.lastname}
                        onChange={handleChange('lastname')}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton edge="end">
                                        <Icon>label</Icon>
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                    />
                    
                    <TextField
                        {...usernameRequiredProp}
                        id="username"
                        className={clsx(classes.root, classes.textField, classes.margin) + ' username'}
                        variant="outlined"
                        label="Username"
                        value={values.username}
                        onChange={handleChange('username')}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton edge="end">
                                        <Icon>account_circle</Icon>
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                    />
                    
                    <TextField
                        {...emailRequiredProp}
                        id="email"
                        className={clsx(classes.root, classes.textField, classes.margin) + ' email'}
                        variant="outlined"
                        label="Email"
                        value={values.email}
                        onChange={handleChange('email')}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton edge="end">
                                        <Icon>email</Icon>
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                    />

                    <TextField
                        {...passwordRequiredProp}
                        id="password"
                        className={clsx(classes.root, classes.textField, classes.margin) + ' password'}
                        variant="outlined"
                        label="password"
                        value={values.password} 
                        type = {!values.visibility? 'password': 'text'}
                        onChange={handleChange('password')}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton edge="end" onClick={handleClickShowPassword}>
                                        {!values.visibility? <Icon>visibility</Icon>: <Icon>visibility_off</Icon>}
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                    />

                    <Button variant="outlined" color="primary" size="large" className={classes.loginButton} type="submit">
                        Register
                        <Icon>arrow_forward</Icon>
                    </Button>
                </form>
                <span>Already a memeber of the site. <Link to="/login" className={classes.link}>Login here!</Link></span>
            </Paper>
        </Fragment>
    );
    
}
 
export default RegisterForm;