import React from 'react';
import { Mutation } from "react-apollo";
import LogoutQuery from "../../ClientQueries/logout";
const LOGOUT_QUERY = LogoutQuery;
function Logout ( props )
{
    return (
        <Mutation mutation={LOGOUT_QUERY}>
            {
                async ( loading, rest) => {
                    
                    console.log( loading );
                    console.log( rest );
                    props.history.push('/');
                }
            }
        </Mutation>
    )
}

export default Logout;