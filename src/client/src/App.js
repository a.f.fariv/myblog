import React from 'react';
import './App.css';
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import Login from "./Components/pages/Login";
import Register from "./Components/pages/Register";
import Home from "./Components/pages/Home";
import Profile from "./Components/pages/Profile";
import Logout from "./Components/pages/Logout";
import Nav from "./Components/Nav";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
// import MeQuery from "./ClientQueries/me";


const client = new ApolloClient({
  uri: "/graphql",
  credentials: "include"
});

// const CHECK_LOGGED_IN_QUERY = MeQuery;

const PrivateRoute = ({component: Component, ...rest}) => {
  console.log( rest );
  console.log( Component );
  let isLoggedIn = client.cache.data.data
  console.log( isLoggedIn );
  console.log( rest );
  return (
      <Route
          {...rest}
          render={props => {
            return rest.noAuth === 1 ? Object.entries(isLoggedIn).length < 1 ?
              (<Component {...props} client={client} />) : 
              (<Redirect to={{
                  pathname: "/", 
                  state: props.location
                }} 
              />) : Object.entries(isLoggedIn).length > 0 ? (<Component {...props} client={client} />) : 
              (<Redirect to={{
                  pathname: "/", 
                  state: props.location
                }} 
              />)
          }
        }
      />
  )
}

function App( props ) {
  return (
    <ApolloProvider client={client}>
      <Router>
        <div className="App">
          <Route path="/*" render={(routeProps) => <Nav {...routeProps} client={client} />} />
          <Switch>
            <Route path="/" exact component={Home} />
            
            <PrivateRoute path="/login" noAuth={1} component={Login} />
            <PrivateRoute path="/register" noAuth={1} component={Register} />
  
            <PrivateRoute path="/logout" noAuth={0} component={Logout} />
            <PrivateRoute path="/profile" noAuth={0} component={Profile} />
            
          </Switch>
        </div>
      </Router>
    </ApolloProvider>
  );
}

export default App;
