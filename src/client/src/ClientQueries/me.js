import { gql } from "apollo-boost";
export default gql`
            {
                me {
                    id
                    username
                    firstname
                    lastname
                    email
                    profile_image
                    ip
                }
            }`;