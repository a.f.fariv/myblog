import { gql } from "apollo-boost";
export default gql`
                mutation createUser(
                    $username: String!
                    $firstname: String
                    $lastname: String!
                    $email: String!
                    $password: String!
                    $ip: String!
                ) {
                    createUser(
                        username: $username
                        firstname: $firstname
                        lastname: $lastname
                        email: $email
                        password: $password
                        ip: $ip
                    ) {
                        id
                        username
                        firstname
                        lastname
                        email
                        profile_image
                        ip
                    }
                }
            `;