import { gql } from "apollo-boost";
export default gql`
        query loginResponse($email: String! $password: String! $ip: String) {
            loginResponse (email: $email password: $password ip: $ip) {
                id
                email
                username
                profile_image
                ip
            }
        }`;